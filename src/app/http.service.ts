import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  // Récupére tous les utilisateurs
  getUsers():Observable<any[]> {
    return this.http.get<any[]>('http://127.0.0.1:8000/api/users')
    .pipe(
      tap(
        data => console.log(data),
        err => console.log(err.message)
      ),
      map(data => data)
    );
  }

  // Récupere l'utilisateur par son id
  getUserById(id):Observable<any[]> {
    return this.http.get<any[]>('http://127.0.0.1:8000/api/users/' + id)
    .pipe(
      tap(
        data => console.log(data),
        err => console.log(err.message)
      ),
      map(data => data)
    );
  }

  setUser(user, currentUserId):Observable<boolean> {
    console.log(user, currentUserId)
    return this.http.put<boolean>('http://127.0.0.1:8000/api/users/' + currentUserId, user)
    .pipe(
      tap(
        data => console.log(data),
        err => console.log(err.message)
      ),
      map(data => data)
    );
  }

  // Supprime un utilisateur
  deleteUser(id):Observable<boolean> {
    return this.http.delete<boolean>('http://127.0.0.1:8000/api/users/' + id)
  }

  // Recupéré tous les rôles
  getRoles():Observable<any[]> {
    return this.http.get<any[]>('http://127.0.0.1:8000/api/roles')
    .pipe(
      tap(
        data => console.log(data),
        err => console.log(err.message)
      ),
      map(data => data)
    );
  }
}
