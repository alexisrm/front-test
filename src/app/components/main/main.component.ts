import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  users: any[] = [];
  roles: any[] = [];
  currentUser = {};
  currentRole = {};
  isDeleted: boolean;
  isUpdated: boolean;
  

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getUsers()
    .subscribe(data => {
      this.users = data;
    })
  }
  // Recupére tous les rôles
  getRoles() {
    this.httpService.getRoles()
      .subscribe(data => {
        this.roles = data;
        console.log(this.roles);
      })
  }

  // updateUser(currentUser) {
  //   this.httpService.setUser(currentUser, this.users)
  //     .subscribe(data => {
  //       this.isSet = data;
  //     })
  // }

  updateUser(userId) {
    for(let user of this.users) {
      if(user.id === userId) {
        this.currentUser = user;
      }
    }
  }

  updateRole(role) {
    this.currentRole = Object.assign(this.currentRole, role);
    //console.log(this.currentRole);
  }

  sendForm(user, currentUserId) {
    //console.log(user);
    let buildUser = user;
    buildUser.roles = this.currentRole;
    this.httpService.setUser(user, currentUserId)
      .subscribe(data => {
         this.isUpdated = data;
      })
      
  }

  // Supprime un utilisateur
  deleteUser(id) {
    this.httpService.deleteUser(id)
      .subscribe(data => {
        this.isDeleted = data;
        for(let i = 0; i < this.users.length; i++) {
          if(this.users[i].id === id) {
            this.users.splice(i);
          }
        }
      })
  }

}
